
figure_diamHeight_timeseries <- function(hddata,type="chamber"){
  
  #- trees that were eventually planted
  trees <- subset(hddata,Date==as.Date("2016-02-03"))$Pot_No
  if(type=="chamber"){
    hddata <- subset(hddata, Pot_No %in% trees)
  }
  
  se <- function(x){
    x <- x[!is.na(x)]
    sd(x)/sqrt(length(x))
  }
  hddata$d2h <- with(hddata,(Diam_65cm/10)^2*Stem_length_cm)
  
  dfa <- summaryBy(. ~ Date + T_treatment, data=hddata, 
                   FUN=c(mean,se))
  
  palette(c("black","red"))
  
  
  #- plot diameter (15-cm)
  with(dfa, plot(Date, Diam_15cm.mean, pch=1, col=T_treatment,
                 ylab="Stem diameter (mm)",
                 ylim=c(min(Diam_15cm.mean-Diam_15cm.se,na.rm=T),max(Diam_65cm.mean+Diam_65cm.se,na.rm=T)),
                 panel.first=adderrorbars(Date, Diam_15cm.mean, 
                                          Diam_15cm.se, direction="updown",
                                          col=T_treatment)
  ))
  
  #- plot diameter (65-cm)
  with(dfa, points(Date, Diam_65cm.mean, pch=19, col=T_treatment,
                 ylab="Stem diameter at 65cm (mm)",
                 ylim=c(min(Diam_65cm.mean-Diam_65cm.se,na.rm=T),max(Diam_65cm.mean+Diam_65cm.se,na.rm=T)),
                 panel.first=adderrorbars(Date, Diam_65cm.mean, 
                                          Diam_65cm.se, direction="updown",
                                          col=T_treatment)
  ))
  axis.Date(side=1,at=seq.Date(from=as.Date("2015-10-1"),to=max(dfa$Date),by="month"))
 
  legend("topleft", legend=c("control: 15cm","warmed: 15cm","control: 65cm","warmed: 65cm"),
         pch=c(1,1,19,19), col=palette(), bty='n', title="Treatment: height") 
  legend("top", "Error bar is 1 SE", bty='n', cex=0.8)
  
  # plot height
  with(dfa, plot(Date, Stem_length_cm.mean, pch=19, col=T_treatment,
                 ylab="Stem length (cm)",
                 ylim=c(min(Stem_length_cm.mean-Stem_length_cm.se,na.rm=T),max(Stem_length_cm.mean+Stem_length_cm.se,na.rm=T)),
                 panel.first=adderrorbars(Date, Stem_length_cm.mean, 
                                          Stem_length_cm.se, direction="updown",
                                          col=T_treatment)
  ))
  axis.Date(side=1,at=seq.Date(from=as.Date("2015-10-1"),to=max(dfa$Date),by="month"))
  
  legend("topleft", levels(dfa$T_treatment), pch=19, col=palette(), bty='n', title="Treatment") 
  legend("top", "Error bar is 1 SE", bty='n', cex=0.8)
  
  # plot d2h
  with(dfa, plot(Date, d2h.mean, pch=19, col=T_treatment,
                 ylab="Volume index (d2h, cm^3)",
                 ylim=c(min(d2h.mean-d2h.se,na.rm=T),max(d2h.mean+d2h.se,na.rm=T)),
                 panel.first=adderrorbars(Date, d2h.mean, 
                                          d2h.se, direction="updown",
                                          col=T_treatment)
  ))
  axis.Date(side=1,at=seq.Date(from=as.Date("2015-10-1"),to=max(dfa$Date),by="month"))
  
  legend("topleft", levels(dfa$T_treatment), pch=19, col=palette(), bty='n', title="Treatment") 
  legend("top", "Error bar is 1 SE", bty='n', cex=0.8)
  
  
  
  
  
  
  
}





#- plot stem volume for each tree (a) and treatment average (b)
plotVol <- function(vol=vol,output=F){
  se <- function(x){
    x <- x[!is.na(x)]
    sd(x)/sqrt(length(x))
  }
  
  dfa <- summaryBy(. ~ Date + T_treatment, data=vol, 
                   FUN=c(mean,se))
  
  windows(50,60);par(mfrow=c(2,1),oma=c(2,3,5,2),mar=c(3,5,1,1),xpd="NA",cex.lab=1.5)
  
  #- plot volume for each chamber
  palette(rev(brewer.pal(12,"Paired")))
  plotBy(vol~Date|chamber,data=vol,type="o",pch=16,legend=F,
         ylab=expression(Stem~volume~(cm^3)),xlab="")
  legend(x=min(vol$Date-5),y=1.4*max(vol$vol),legend=levels(vol$chamber),fill=palette()[1:12],ncol=6)  
  
  
  #- plot treatment mean volume
  palette(c("black","red"))
  with(dfa, plot(Date, vol.mean, pch=19, col=T_treatment,las=1,
                 ylab=expression(Stem~volume~(cm^3)),
                 ylim=c(min(vol.mean-vol.se,na.rm=T),max(vol.mean+vol.se,na.rm=T)),
                 panel.first=adderrorbars(Date, vol.mean, 
                                          vol.se, direction="updown",
                                          col=T_treatment)
  ))
  #axis.Date(side=1,at=seq.Date(from=as.Date("2015-10-1"),to=max(dfa$Date),by="month"))
  legend("topleft", levels(dfa$T_treatment), pch=19, col=palette(), bty='n', title="Treatment") 
  legend("top", "Error bar is 1 SE", bty='n', cex=0.8)
  if(output==T) dev.copy2pdf(file="output/figure_stemVolume.pdf")
}
