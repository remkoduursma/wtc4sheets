# Automated data streaming from the WTC-4

This repository contains code to process raw height and diameter data from the WTC4.

- Read raw data from HIEData2 drive
- The HIEData2 drive should be mapped to a drive (by default, "W")
- Combine into a single (slightly edited) CSV
- Make figure of diameter vs Date by treatment
- Email

## Set up

1. If you want to send emails, place password.txt in the working directory, containing the password for wtcdatastream@gmail.com (Do NOT commit that file!).
2. Java runtime environment needs to be installed (required by the `mailR` package) (make sure to install 64bit if that's your system)
3. All other packages will be installed from the `R/load.R` script.
4. At the top of `run.R`, change some settings, most notably whether to email the figures and the mapping of the HIEData2 drive.
5. Source `run.R` to run.

### Notes

Filename on HIEv will be "WTC_TEMP-PARRA_CM_TREE-HEIGHT-DIAMETER_OPEN_L1.csv". See [HIEv data protocol](http://hie-dm.uws.edu.au/wtc-data-protocols/) for use of the (new) 'OPEN' tag for files that are continually added to.
